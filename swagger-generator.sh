#!/bin/bash
echo '*** fetching swagger-django-generator submodule' && \
    git submodule update --init swagger-django-generator && \
    echo '*** installing swagger generator deps' && \
    pip install -r swagger-django-generator/requirements.txt && \
    echo '*** chdiring inside' && \
    cd swagger-django-generator/ && \
    echo '*** generating' && \
    python ./swagger_django_generator/generator.py ../swagger.yaml --output-dir ../api --module-name api && \
    echo '*** success!'
cd ..
