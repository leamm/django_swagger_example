# Usage instructions 

## Virtual environment creation (pyenv, pyenv 
```
pyenv install 3.6.6
pyenv virtualenv 3.6.6 django_swagger_example
pyenv local django_swagger_example
pip install 'pip<10' # this is a workaround to a bug in django-swagger-generator
pip install -r requirements.txt
```

In the given repository a Django project already exists.
created the following way (__no need to execute__): 
```
django-admin startproject django_swagger_example
```

## To generate an API carcass with https://github.com/guyromm/swagger-django-generator
```
mkdir ./api
./swagger-generator.sh
```

## Pull swagger ui & switch the api/ui index
```
git submodule update --init swagger-ui
cd api/ && ln -s ../swagger-ui/dist ./ui && cd -
sed "s/'ui_index.html/ 'ui', 'index.html/" -i api/views.py
sed "s/https:\/\/petstore\.swagger\.io\/v2\/swagger\.json/\/api\/the_specification/" -i api/ui/index.html
```

## Run dev server
```
./manage.py migrate
./manage.py runserver
```

## Endpoints
| What       | URL                              |
|------------|-------------------------------|
| Swagger UI | http://127.0.0.1:8000/api/ui/ |
| Admin      | http://127.0.0.1:8000/admin/  |

